package com.pimlada.week11;

public class Bus extends Vehicle{
    public Bus(String name, String engine) {
        super(name, engine);
    }

    @Override
    public String toString() {
        return "Bus (" + getName() + ")";
    }

    public void print() {
        System.out.println("Bus (" + getName() + ")");
    }
}
