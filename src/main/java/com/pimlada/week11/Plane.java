package com.pimlada.week11;

public class Plane extends Vehicle implements Flyable{

    public Plane(String name, String engine) {
        super(name, engine);
    }

    @Override
    public String toString() {
        return "Plane (" + getName()+ ")";
    }

    @Override
    public void takeoff() {
        System.out.println(this + " takeoff.");
    }

    @Override
    public void fly() {
        System.out.println(this + " fly.");
    }

    @Override
    public void landing() {
        System.out.println(this + " landing.");
    }
    
}
