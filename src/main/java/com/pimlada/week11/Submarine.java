package com.pimlada.week11;

public class Submarine extends Vehicle implements Swimable{
    public Submarine(String name, String engine) {
        super(name, engine);
    }

    @Override
    public String toString() {
        return "Submarine (" + getName()+ ")";
    }

    @Override
    public void swim() {
        System.out.println(this + " swim."); 
    }
}
